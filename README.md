## Required Software

### Git

Download git from https://git-scm.com/downloads and install it with the default options. If you're on OSX install using brew.

### VSCode

Download VSCode from https://code.visualstudio.com/ and install it with the default options.

### rustup

Download rustup from https://rustup.rs/ and install it with the default options. If you're on OSX, launch VSCode and from the menubar select `Terminal > New Terminal` and paste the command there.

### Git Config

First, launch VSCode and from menubar select `Terminal > New Terminal`.

**If you're on Windows** use the menu indicated in the image below to click `Select Default Shell`; When prompted set your shell to `Git Bash`.

![](_assets/vscode-terminal-select.png)

The remainder of the setup involves pasting commands into the terminal. When editing in the terminal you cannot use your mouse to move the cursor, you will need to use the arrow keys.

Here is what your screen should look like after you finish the instructions below:

![](_assets/git-config.png)

Paste the following two commands into your terminal one at a time, substituting your own information. Use the same email which you signed up for GitLab with.

```sh
git config --global user.name "Firstname Lastname"
git config --global user.email "you@example.com"
```

Paste the following command into your terminal to generate a secure key which GitLab will use to identify your computer when you try to make changes. Save the key in the default location and leave the password empty when prompted for one.

```sh
ssh-keygen -t rsa -b 4096
```

Finally run the below command and paste its output into https://gitlab.com/-/profile/keys so GitLab knows this key is yours. There is no need to set an expiration date.

```sh
cat ~/.ssh/id_rsa.pub
```
