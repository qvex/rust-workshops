---
aspectratio: 1610
fontsize: 10pt
---

# Rust in a Nutshell

::: columns

:::: column

- (Very) Strongly typed
- Multi-paradigm
- Designed for performance
- Not garbage collected
- "Batteries Included"
    - Large standard library
    - Build tool
    - Formating tool
    - IDE support

::::

:::: column

![Rust's Mascot: Ferris](assets/ferris.png)

::::

:::

# Why Rust Instead of C++?

- It's Easier to teach
- It's a more "modern" language
- We hope it will offer a better developer experience
- The Rust community maintains many amazing (and up-to-date) online resources
    - [https://doc.rust-lang.org/book/](https://doc.rust-lang.org/book/)
    - [https://doc.rust-lang.org/rust-by-example/](https://doc.rust-lang.org/rust-by-example/)
    - [https://doc.rust-lang.org/std/](https://doc.rust-lang.org/std/)

# Setting Up The Workshop Repo

1. Launch VSCode
2. Press `View > Command Palette`
3. Type `Git: Clone`
4. Paste `git@gitlab.com:qvex/rust-workshops.git`
5. Select a location to save

# Hello World

First create a new project using Rust's build tool: `cargo`. Launch the terminal (`Terminal > New Terminal`) and run the below command.

```sh
$ cargo new hello-world
```

**Note:** the `$` is just so you know this is a terminal command. Do not type it into your terminal or you will get an error.

Finally add the project to the workspace by modifying the root `Cargo.toml`.

```
[workspace]

members = [
    "hello-world"
]
```

# Hello World (Cont.)

Cargo has has created several new directories and files. The one we are most interested is `hello-world/src/main.rs`, which is the entrypoint for our hello-world program. Let's open this file using the VSCode file tree.

We can compile and run the program using `cargo`.

```sh
$ cargo run --bin hello-world
```

We can also compile and run by clicking the `Run` button above the `main` function.

# Commiting our Changes

Use VSCode Source Control to:

1. Create a new branch `firstname-lastname-year-month`
2. Stage changes
3. Commit changes
4. Push branch

# Guessing Game

Let's jump into rust by working on a hands-on project. There will be a lot of new info here, so try to follow along and let me know if I'm going too fast.

Here's the project: our program will generate a random integer between 1 and 100. It will prompt the player to enter a guess. After a guess is entered, the program will indicate wheather the guess is too low or too high. If the guess is correct the game will print a congratulatory message and exit.

# Setting up a New Project

Let's begin by using `cargo` to create a new project.

```sh
$ cargo new guessing-game
```

And don't forget to add the project to the workspace.

```
[workspace]

members = [
    "hello-world",
    "guessing-game"
]
```

# Processing a Guess

```rs
use std::io;

fn main() {
    println!("Guess the number!");

    println!("Please input your guess.");

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
}
```

# Adding a Dependency

```toml
[package]
name = "guessing-game"
version = "0.1.0"
authors = ["Timothy Morland <thetimmorland@gmail.com>"]
edition = "2018"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
rand = "0.6.0"
```

# Generating a Random Number

```rs
use std::io;
use rand::Rng;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    println!("The secret number is: {}", secret_number);

    println!("Please input your guess.");

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
}
```

# Comparing the Guess to a Random Number

```rs
    // --snip--

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    let guess: u32 = guess.trim().parse().expect("Please type a number!");

    println!("You guessed: {}", guess);

    match guess.cmp(&secret_number) {
        Ordering::Less => println!("Too small!"),
        Ordering::Greater => println!("Too big!"),
        Ordering::Equal => println!("You win!"),
    }
}
```

# Allowing Multiple Guesses with Looping

```rs
    // --snip--

    println!("The secret number is: {}", secret_number);

    loop {
        println!("Please input your guess.");

        // --snip--

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => println!("You win!"),
        }
    }
}
```

# Quitting After a Correct Guess

```rs
        // --snip--

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
```

# Handling Invalid Input

```rs
        // --snip--

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guessed: {}", guess);

        // --snip--
```

# Summary

At this point, hopefully everyone's guessing game is working.

Make sure to commit and push your work!

This example was shamelessly ripped from chapter 2 of The Rust Book ([https://doc.rust-lang.org/book/](https://doc.rust-lang.org/book/)).

If there's something I explained poorly, it might be a good idea to look for a better explanation in the book.

# Exercise

Create a new project called `string-count` which prompts the user for a file path and a string. The program should then count the number of occurrences of that string in the file, and print the count to the user.

You can test your program with this Repo's README like so:

```
$ cargo run --bin string-count
Enter a path:
README.md
Enter a string:
git
File `README.md` has 12 occurrences of string `git`.
```

Matches should case insensitive. Matches can happen in the middle of a word. Eg: `GitLab` still counts as an occurrence of `git`.

# State Diagram

[![](https://mermaid.ink/img/eyJjb2RlIjoic3RhdGVEaWFncmFtLXYyXG4gICAgWypdIC0tPiBHRVRfRklMRU5BTUVcbiAgICBHRVRfRklMRU5BTUUgLS0-IEdFVF9TVFJJTkdcbiAgICBHRVRfU1RSSU5HIC0tPiBQUklOVF9SRVNVTFRcbiAgICBQUklOVF9SRVNVTFQgLS0-IFsqXVxuICAgIFxuICAgIHN0YXRlIEdFVF9GSUxFTkFNRSB7XG4gICAgICAgIFsqXSAtLT4gUFJPTVBUXG4gICAgICAgIFBST01QVCAtLT4gVkFMSURBVEVcbiAgICAgICAgVkFMSURBVEUgLS0-IFsqXSA6IHZhbGlkXG4gICAgICAgIFZBTElEQVRFIC0tPiBQUk9NUFQgOiBpbnZhbGlkXG4gICAgfVxuICAgXG4gICAgc3RhdGUgR0VUX1NUUklORyB7XG4gICAgICAgIFsqXSAtLT4gX1BST01QVFxuICAgICAgICBfUFJPTVBUIC0tPiBfVkFMSURBVEVcbiAgICAgICAgX1ZBTElEQVRFIC0tPiBbKl0gOiB2YWxpZFxuICAgICAgICBfVkFMSURBVEUgLS0-IF9QUk9NUFQgOiBpbnZhbGlkXG4gICAgfSIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoic3RhdGVEaWFncmFtLXYyXG4gICAgWypdIC0tPiBHRVRfRklMRU5BTUVcbiAgICBHRVRfRklMRU5BTUUgLS0-IEdFVF9TVFJJTkdcbiAgICBHRVRfU1RSSU5HIC0tPiBQUklOVF9SRVNVTFRcbiAgICBQUklOVF9SRVNVTFQgLS0-IFsqXVxuICAgIFxuICAgIHN0YXRlIEdFVF9GSUxFTkFNRSB7XG4gICAgICAgIFsqXSAtLT4gUFJPTVBUXG4gICAgICAgIFBST01QVCAtLT4gVkFMSURBVEVcbiAgICAgICAgVkFMSURBVEUgLS0-IFsqXSA6IHZhbGlkXG4gICAgICAgIFZBTElEQVRFIC0tPiBQUk9NUFQgOiBpbnZhbGlkXG4gICAgfVxuICAgXG4gICAgc3RhdGUgR0VUX1NUUklORyB7XG4gICAgICAgIFsqXSAtLT4gX1BST01QVFxuICAgICAgICBfUFJPTVBUIC0tPiBfVkFMSURBVEVcbiAgICAgICAgX1ZBTElEQVRFIC0tPiBbKl0gOiB2YWxpZFxuICAgICAgICBfVkFMSURBVEUgLS0-IF9QUk9NUFQgOiBpbnZhbGlkXG4gICAgfSIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)

# A Pattern for Validating Input

```rs
let input = loop {
    println!("Enter something:");

    let mut s = String::new();
    io::stdin()
        .read_line(&mut s)
        .expect("Failed to read line");

    // pop newline off of end of input
    s.pop();

    if s == "" {
        println!("Error: input is empty.");
    } else {
        break s;
    };
}
```

# A Function for Reading Files to String

```rs
fn cat(path: &str) -> io::Result<String> {
    let mut f = File::open(path)?;
    let mut s = String::new();
    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}
```

# Tips

- The standard library has lots of methods on `String`
- Make use of Rust community resources:
    - [https://doc.rust-lang.org/book/](https://doc.rust-lang.org/book/)
    - [https://doc.rust-lang.org/rust-by-example/](https://doc.rust-lang.org/rust-by-example/)
    - [https://doc.rust-lang.org/std/](https://doc.rust-lang.org/std/)
- Google questions first then ask me.

# Submitting Solution

1. Commit your changes
2. Push your branch
3. Create a Merge Request: https://gitlab.com/qvex/rust-workshops/-/merge_requests/new
4. Title your MR: Firstname Lastname String Count
5. Select yourself as Assignee
6. Select Timothy Morland as Reviewer
7. I will review and either approve or ask for changes
